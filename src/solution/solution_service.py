import uuid

import pandas as pd
from loguru import logger

from src import cofing
from src.ml.KNNClassificator import KNNClassificator
from src.redis.methods import set_request_data


class SolutionService:

    def __init__(self):
        self.model = KNNClassificator(cofing.DATA_DIR / "diabetes.csv")

    def solve_problem(self, request_id: uuid.UUID, df: pd.DataFrame):
        # Save request data in Redis

        logger.info("Start processing request %s" % str(request_id))
        result = self.model.predict(df)

        df['outcome'] = [result]
        set_request_data(request_id, df)

        return result
