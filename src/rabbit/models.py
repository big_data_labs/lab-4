from pydantic import BaseModel


class ModelRequestRabbitMQ(BaseModel):
    request_id: str
    pregnancies: int
    glucose: int
    blood_pressure: int
    skin_thickness: int
    insulin: int
    bmi: int
    diabetes_pedigree_function: float
    age: float
