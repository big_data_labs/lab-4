import json

import aio_pika

from src.cofing import get_settings

settings = get_settings()


async def publish(routing_key: str, payload: dict) -> None:
    connection = await aio_pika.connect_robust(settings.rabbit.uri)

    async with connection:
        channel = await connection.channel()
        exchange = await channel.declare_exchange(settings.rabbit.exchange, aio_pika.ExchangeType.DIRECT)

        response = await exchange.publish(
            aio_pika.Message(body=json.dumps(payload).encode()),
            routing_key=routing_key,
        )

        print(response)
