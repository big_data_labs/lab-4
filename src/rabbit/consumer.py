import json

import aio_pika
import pandas as pd
from loguru import logger
from pydantic import ValidationError

from src.api.app import solution_server
from src.cofing import get_settings
from src.rabbit.models import ModelRequestRabbitMQ

settings = get_settings()


async def consumer(routing_key: str) -> None:
    connection = await aio_pika.connect_robust(settings.rabbit.uri)

    async with connection:
        channel = await connection.channel()
        exchange = await channel.declare_exchange(
            settings.rabbit.exchange, aio_pika.ExchangeType.DIRECT
        )

        queue = await channel.declare_queue(settings.rabbit.queue, durable=True)
        await queue.bind(exchange, routing_key=routing_key)

        async with queue.iterator() as queue_iter:
            async for message in queue_iter:
                async with message.process():
                    logger.info("Recieved new message")
                    raw_data = json.loads(message.body.decode())

                    try:
                        model_request = ModelRequestRabbitMQ.validate(raw_data)
                        df = pd.DataFrame({
                            'Pregnancies': [model_request.pregnancies],
                            'Glucose': [model_request.glucose],
                            'BloodPressure': [model_request.glucose],
                            'SkinThickness': [model_request.skin_thickness],
                            'Insulin': [model_request.insulin],
                            'BMI': [model_request.bmi],
                            'DiabetesPedigreeFunction': [model_request.diabetes_pedigree_function],
                            'Age': [model_request.age]
                        })
                        solution_server.solve_problem(model_request.request_id, df)
                    except ValidationError:
                        logger.warning("Not ModelRequestRabbitMQ get as message")
