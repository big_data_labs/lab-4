import argparse
import asyncio

import uvicorn
from loguru import logger

from src.cofing import get_settings

from src.rabbit.consumer import consumer
from src.rabbit.publisher import publish

if __name__ == "__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument(
        'app_type',
        choices=['api', 'producer', 'consumer'],
        help='Type of application to start'
    )

    args = parser.parse_args()
    get_settings()

    match args.app_type:
        case "api":
            logger.info("Start API")
            uvicorn.run("src.api.app:app", host="0.0.0.0", port=8000, log_level="info")
        case "producer":
            logger.info("Start produce messages")
            publish(routing_key='test', payload={"lol": "hello"})
        case "consumer":
            logger.info("Start consumer")
            asyncio.run(consumer(routing_key='model'))
        case _:
            logger.warning("Nothing to start")
