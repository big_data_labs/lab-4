# Lab 4

## Описание:
Этот проект является форком проекта lab_3. В проект было добавлено использование Kafka броекра

Цель этого проекта — изучить и реализовать взаимодействие между RabbitMQ Producer и Consumer, а также их последующую интеграцию в платформу API для решения задач машинного обучения. Проект использует Docker Compose для настройки и координации нескольких сервисов, включая Redis в качестве базы данных, RabbitMQ как брокер сообщений, FastAPI для создания API сервиса и Consumer для выполнения задач, поступающих из брокера сообщений.
Состав проекта

### Проект включает следующие компоненты:

- **API сервис** (FastAPI): Обеспечивает REST API для взаимодействия с внешними системами и отправки задач в очередь RabbitMQ.

- **rabbitMQ**: Брокер сообщений, который принимает задачи от API сервиса и передает их Consumer'у.

- **consumer**: Обрабатывает задачи, полученные из очереди RabbitMQ, и выполняет соответствующие действия, такие как запуск ML моделей.

- **redis**: Используется как база данных для хранения состояния задач и результатов их выполнения.

![](imgs/img.png)

## Запуск

Запуск контейнеров

```shell
docker-compose up -d --build
```