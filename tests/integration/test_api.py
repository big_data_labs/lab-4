import pytest

from src.api.app import app
from src.api.models import PredictResponse
from fastapi.testclient import TestClient


@pytest.mark.asyncio
async def test_predict_api(api_url, sample_input_data):
    client = TestClient(app)

    response = client.post('/predict', json=sample_input_data.dict())

    assert response.status_code == 200
    response_data = PredictResponse.parse_obj(response.json())
    assert isinstance(response_data.outcome, bool)
