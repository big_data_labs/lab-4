import pytest

from src import cofing
from src.ml.KNNClassificator import KNNClassificator


@pytest.fixture(scope='session')
def knn_model():
    return KNNClassificator(cofing.DATA_DIR / "diabetes.csv")
