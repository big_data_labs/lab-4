#!/bin/sh

ansible-vault decrypt vault.yml --vault-password-file ./vault_password_file
python3 -m src api
