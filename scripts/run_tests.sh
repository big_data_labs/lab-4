#!/bin/sh


ruff check src tests

ansible-vault decrypt vault.yml --vault-password-file ./vault_password_file
pytest -v tests
ansible-vault encrypt vault.yml --vault-password-file ./vault_password_file